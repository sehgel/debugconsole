extends Node

var control : Control
var spawned = false
signal on_spawned
signal on_despawned

func toggle():
	if spawned:
		hide()
	else:
		show()
func show():
	spawned = true
	animate(-get_tree().root.size.y,0)
func hide():
	spawned = false
	animate(0,-get_tree().root.size.y)
func animate(start, end):
	var counter = 0.0
	while counter < 1:
		counter += get_process_delta_time()*10
		control.rect_position.y = lerp(start,end,counter)
		yield(get_tree(),"idle_frame")
	control.rect_position.y = end
	
	if spawned:
		emit_signal("on_spawned")
	else:
		emit_signal("on_despawned")
		