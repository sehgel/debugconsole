extends LineEdit

onready var prediction_label = get_node("PredictionLabel")


func set_prediction_text(prediction_text : String, start_index : int):
	var result = ""
	for i in range(start_index):
		result += " "
	prediction_label.text = result + prediction_text.right(start_index)
func set_hint_text(hint_one_liner : String, command_size : int):
	var result = " "
	for i in range(command_size):
		result += " "
	prediction_label.text = result + hint_one_liner
func reset_prediction():
	prediction_label.text = ""
#console animation callbacks
func on_console_spawn():
	grab_focus()
	move_caret_to_end()
func on_console_despawn():
#	print("despawn")
	release_focus()
	pass
func move_caret_to_end():
	caret_position = text.length()