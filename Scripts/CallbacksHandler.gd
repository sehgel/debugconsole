const NOT_ENOUGH_ARGUMENTS = "ERROR. Not enough arguments for command..."

var commands = {
}
func register_callback(cmd_name : String, func_ref : FuncRef, hints : PoolStringArray = PoolStringArray()):
	var cmd = CMD.new()
	cmd.func_ref = func_ref
	cmd.hints = hints
	commands[cmd_name] = cmd
	print("Command: %s added." % cmd_name)


func process_command(cmd : String):
	var first_space = cmd.find(" ")
	var cmd_name = cmd
	var arguments = PoolStringArray()
	if first_space != -1:
		cmd_name = cmd.left(first_space)
		var splitted = cmd.right(first_space+1).split(" ")
		arguments = splitted
	if not commands.has(cmd_name):
		return
	if arguments.size() < commands[cmd_name].hints.size():
		Console.append_to_record(NOT_ENOUGH_ARGUMENTS)
		return
	commands[cmd_name].func_ref.call_func(arguments)
		
func has_command(command : String) -> bool:
	for key in commands.keys():
		if key == command:
			return true
	return false
func get_all_hints(cmd_name : String):
	return commands[cmd_name].hints
func get_hints_one_liner(cmd_name : String):
	var result = ""
	for hint in commands[cmd_name].hints:
		result += "%s "%hint
	return result.left(result.length()-1)
func get_similar_commands(sub_string : String, skips = 0):
	for key in commands.keys():
		var all_chars_checked = true
		for i in range(sub_string.length()):
			if i < key.length() and sub_string[i] != key[i]:
				all_chars_checked = false
				break
		if all_chars_checked:
			if skips <= 0:
				return key
			skips -= 1
	return sub_string
func print_all_commands(args):
	Console.append_to_record("Registered Commands:")
	for i in range(commands.size()):
		var hint_line = ""
		for hint in commands.values()[i].hints:
			hint_line += "%s," % hint
		Console.append_to_record("$%s = (%s)" % [commands.keys()[i],hint_line.left(hint_line.length()-1)])

func _init():
	var cmd = CMD.new()
	cmd.func_ref = funcref(self,"print_all_commands")
	commands["help"] = cmd
class CMD:
#	var arguments : PoolStringArray #space separated
	var func_ref : FuncRef
	var hints : PoolStringArray
