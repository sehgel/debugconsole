extends Node

func setup():
	Console.register_command("showcollisions",funcref(self,"toggle_collisions_visibility"), ["bool"])
	Console.register_command("fullscreen",funcref(self,"fullscreen"))
	Console.register_command("targetfps",funcref(self,"target_fps"), ["int"])
	Console.register_command("changescene",funcref(self,"change_scene"), ["scene_path"])
	Console.register_command("showsettings",funcref(self,"show_settings"))
	Console.register_command("setsetting",funcref(self,"set_setting"),["section","key","value"])
	
func set_setting(args : PoolStringArray):
	if not Console.settings.has(args[0]):
		Console.append_to_record("Couldn't found section: %s" % args[0])
		return
	if not Console.settings[args[0]].has(args[1]):
		Console.append_to_record("Couldn't found key: %s" % args[1])
		return
	Console.settings[args[0]][args[1]] = args[2]
	Console.save_settings()
	Console.apply_settings()
	Console.append_to_record("[%s]\n-%s: %s" % Array(args))

func show_settings(args : PoolStringArray):
	for section in Console.settings.keys():
		Console.append_to_record("[%s]" % section)
		for key in Console.settings[section].keys():
			Console.append_to_record("-%s: %s" % [key,Console.settings[section][key]])
			
func change_scene(args : PoolStringArray):
	get_tree().change_scene("%s.tscn" % args[0])
	Console.move_to_last_in_tree()
	
func target_fps(args):
	Engine.target_fps = int(args[0])
	
func fullscreen(args):
	OS.window_fullscreen = not OS.window_fullscreen

func toggle_collisions_visibility(arguments : PoolStringArray):
	Console.append_to_record("This command may take some time, depending on the number of nodes you have in scene...")
	var value = arguments[0] == "true"
	set_visibility_of_all_children(value,get_tree().root)
	
#	thread.start(self, "threaded_set_visibility_of_children",value)
#	thread.wait_to_finish()
	
func threaded_set_visibility_of_children(value):
	set_visibility_of_all_children(value,get_tree().root)

func set_visibility_of_all_children(visibility : bool, parent : Node):
	if parent.get_child_count() <= 0:
		return
	for child in parent.get_children():
		if child is CollisionShape2D or child is CollisionShape or child is RayCast2D or child is RayCast:
			child.visible = visibility
#			Console.append_to_record("%s visibility %s." % [child.name,"enabled" if visibility else "disabled"])
		set_visibility_of_all_children(visibility,child)