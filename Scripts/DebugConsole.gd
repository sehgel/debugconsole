extends Panel

export(int) var side_margins = 25
export(int) var bottom_margin = 10

onready var record = get_node("Record")
onready var line_edit = get_node("LineEdit")
onready var animation_module = get_node("Animation")
onready var callbacks_handler = preload("res://DebugConsole/Scripts/CallbacksHandler.gd").new()



#prediction variables
var prediction = PredictionData.new()

func append_to_record(text):
	record.append_bbcode("%s\n" % text)

func register_command(name_and_arguments : String, func_ref : FuncRef, hints : PoolStringArray = PoolStringArray()):
	callbacks_handler.register_callback(name_and_arguments,func_ref,hints)
func enter(text):
	append_to_record("->%s" % text)
	callbacks_handler.process_command(text)
	line_edit.text = ""
	prediction.last_command = text
	line_edit.reset_prediction()
	
func changed(text):
	prediction.next_index = 0
	#If empty, clear theres nothing to predict, returns...
	if text.length() <= 0:
		line_edit.reset_prediction()
		return
	
	#search for the next similar command
	var next_similar_text = callbacks_handler.get_similar_commands(text,prediction.next_index)

	#Did not reach a full command yet, try to predict
	if next_similar_text.find(text) != -1:
		line_edit.set_prediction_text(next_similar_text,line_edit.text.length())
	else:#Found a full command, try to get hints
	
		#Allow to have one space at the end for hint readibility
		var number_of_hints_filled = 0
		if text.length() > 0 and text.find(" ") != -1:
			var splitted = text.split(" ")
			text = splitted[0]
		
		if callbacks_handler.has_command(text):
			#Get all hints
			var hints = callbacks_handler.get_all_hints(text)
			#Check how many spaces you have inputed
			var spaces_count = line_edit.text.split(" ").size() -1
			#calculate result hint removing already typed arguments
			var result_hint = ""
			var last_index = line_edit.text.find_last(" ")
			if line_edit.text.length() - last_index > 1:
				spaces_count += 1#If you inputed a character after a space, remove that hint
			#put all remainder hints together
			for i in range(spaces_count-1,hints.size()):
				result_hint += "%s "% hints[i]
			#print them
			line_edit.set_hint_text(result_hint,line_edit.text.length()-1)
		else:#Found no hints, reset prediction
			line_edit.reset_prediction()
	
func predict(text):#Button pressed TAB
	#if nothing in enter, then reinput the last commmand submitted
	if text.length() <= 0:
		line_edit.text = prediction.last_command
	else:#If something is entered, try to get the next similar command on the dictionary
		var new_text = callbacks_handler.get_similar_commands(text,prediction.next_index)
		prediction.next_index += 1
		line_edit.text = new_text
	
	line_edit.move_caret_to_end()
	changed(line_edit.text)
	
func _input(event):
	if event is InputEventKey:
		if event.pressed and not event.echo:
			if event.scancode == KEY_QUOTELEFT:
				line_edit.release_focus()
				animation_module.toggle()
			if event.scancode == KEY_TAB:
				predict(line_edit.text)
func _ready():
	load_settings()
	save_settings()
	#Setup dependencies
	$BaseCommands.setup()
	
	animation_module.control = self
	animation_module.hide()
	#Re ajust on resize
	get_tree().root.connect("size_changed",self,"window_resized")
	window_resized()
	
	#handle signals
	line_edit.connect("text_entered",self,"enter")
	line_edit.connect("text_changed",self,"changed")
	animation_module.connect("on_spawned",line_edit,"on_console_spawn")
	animation_module.connect("on_despawned",line_edit,"on_console_despawn")
	
	move_to_last_in_tree()
func move_to_last_in_tree():
	yield(get_tree().create_timer(0.1),"timeout")#WORKAROUND
	get_parent().move_child(self,get_parent().get_child_count())
func window_resized():
	margin_left = side_margins
	margin_bottom = bottom_margin
	rect_size = get_tree().root.size - Vector2(side_margins*2,bottom_margin)
	record.rect_size = Vector2(rect_size.x,rect_size.y + line_edit.margin_top)
	
	
class PredictionData:
	var next_index = 0
	var last_command = ""
	

#PERSISTENCE
const SAVE_PATH = "res://DebugConsole/config.cfg"
var config_file = ConfigFile.new()
onready var settings = {
	"console":{
		"side_margins": side_margins,
		"bottom_margin": bottom_margin,
	},
	"window":{
		"width": OS.window_size.x,
		"height": OS.window_size.y,
	},
	"game":{
		"width": get_tree().root.size.x,
		"height": get_tree().root.size.y,
	},
	"record":{
		"font-size": 25
	}
}
func save_settings():
	for section in settings.keys():
		for key in settings[section].keys():
			config_file.set_value(section,key,settings[section][key])
			
	config_file.save(SAVE_PATH)
func load_settings():
	var error = config_file.load(SAVE_PATH)
	if error != OK:
		append_to_record("Error loading configuration file. Error code: %s" % error)
		return
	
	for section in settings.keys():
		for key in settings[section].keys():
			settings[section][key] = config_file.get_value(section,key,settings[section])
	load_persistent_data()
func apply_settings():
	load_persistent_data()
func load_persistent_data():
	#Console
	side_margins = int(settings["console"]["side_margins"])
	bottom_margin = int(settings["console"]["bottom_margin"])
	#Window
	OS.window_size.x = int(settings["window"]["width"])
	OS.window_size.y = int(settings["window"]["height"])
	#Game
	get_tree().get_root().size.x = int(settings["game"]["width"])
	get_tree().get_root().size.y = int(settings["game"]["height"])
	
	window_resized()
	